#include "dimwrapper.h"
#include "time.h"
#include "unistd.h"
#include <stdio.h>
int buffer[] = { 0,1,2,3,4,5,6,7,8,9 };  
int service_id;  
  
void build_service(tag, address, size)  
long *tag;  
int **address;  
int *size;  
{    
    *address = buffer;  
    *size = sizeof(buffer);  
}    

void execute_cmnd(long *tag, char *cmnd_buffer, int *size)  
{  
    if(*tag == 1) {  
        printf("SERV_CMND: Command %s received\n",cmnd_buffer);  
        w_dis_update_service(service_id);  
    }  
}  
  
int main()  
{  

    w_dis_add_service("SERV_BY_BUFFER", "L", buffer, 40, 0, 0);  
    service_id = w_dis_add_service("SERV_BY_ROUTINE", "L", 0, 0,  
        build_service, 0);  
      w_dis_start_serving("DIS_TEST");  
    for (;;) {
        usleep(10);  
    }  
}