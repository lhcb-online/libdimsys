#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

/// The module contains the "raw" genereated bidnings
mod raw {
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

// export all
pub use crate::raw::*;

#[cfg(test)]
mod tests {
    
    static mut ctr: c_int = 0;
    use crate::raw::*;
    use std::mem::size_of;
    use std::os::raw::{c_long, c_char, c_int};
    use std::{ffi::{CString, c_void, CStr}, time::Duration};
    /*

    void execute_cmnd(long *tag, char *cmnd_buffer, int *size)  
{  
    if(*tag == 1) {  
        printf("SERV_CMND: Command %s received\n",cmnd_buffer);  
        w_dis_update_service(service_id);  
    }  
}  
     */
    #[no_mangle]
    unsafe extern fn  execute_cmnd(tag: *mut c_long, cmnd_buffer: *mut c_char,  size: *mut c_int) {
        if *tag == 1 {
            let str_res =  CStr::from_ptr(cmnd_buffer).to_str().unwrap();
            println!("Command buffer reciebved {}",str_res);
        }
    
}
    #[no_mangle]
    unsafe extern fn provide_buffer(tag: *mut c_long, adress: *mut *mut ::std::os::raw::c_int, size: *mut ::std::os::raw::c_int)  {
        ctr += 1;
        *adress = &mut ctr;
        *size = size_of::<c_int>().try_into().unwrap();
    }
    //#[test]
//    fn it_works() {
        
        //let mut c_str =  CString::new("HICKSO").unwrap();
       //let server_desc = CString::new("C:").unwrap();
       // let buf_desc = CString::new("L").unwrap();
      //  let mut server_command_name = CString::new("HICKSO_CMND").unwrap();
      //  let mut buf_srv_name = CString::new("HCIKSO_BUFFER_TIME").unwrap();
        //let mut serv_id = 0;
  //      unsafe {
    //    w_dis_add_cmnd(server_command_name.as_ptr(), server_desc.as_ptr(), Some(execute_cmnd), 1);  
      //  serv_id  = w_dis_add_service(buf_srv_name.as_ptr(), buf_desc.as_ptr(), 0 as *mut c_int, 0,   Some(provide_buffer), 0);  
       // w_dis_start_serving(c_str.as_ptr());
   // }
   // unsafe {
    //loop {
     //   std::thread::sleep(Duration::from_millis(10));
      //  w_dis_update_service(serv_id);
   // }
    //}
    //}
}
