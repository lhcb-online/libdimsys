use std::{path::PathBuf, env};
use cc::Build;
use std::process::Command;
fn main() {
    eprintln!("Building trampoline library");
    let make_cmd = Command::new("make").arg("clean").arg("libdimwrapper.a").spawn().expect("You need make command");
    println!("cargo:rustc-link-search=."); // for static lib
    println!("cargo:rustc-link-lib=static=dimwrapper");
    println!("cargo:rustc-link-lib=dylib=dim");
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("wrapper.h")
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");

}
