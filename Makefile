CFLAGS=-Wall
LDFLAGS=-shared -L$(DIMROOT)/linux
LD=ld
CC=cc

all: libdimwrapper.a testclient

libdimwrapper.so: dimwrapper.o
	$(LD) $(LDFLAGS) -o $(@) -ldim -lpthread $+

libdimwrapper.a: dimwrapper.o
	ar rcs  $(@) $+ 

testclient: testclient.c libdimwrapper.so 
	$(CC) -o $@ $+

.PHONY: clean

clean:
	$(RM) *.o
