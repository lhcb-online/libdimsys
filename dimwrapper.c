#include "dimwrapper.h"
#include <dim/dis.h>

unsigned int w_dis_add_cmnd(const char *name, const char *description, cmd_callback user_routine, long tag) {
    return dis_add_cmnd(name, description, user_routine, tag);
}

unsigned int w_dis_add_service(const char *name, const char *description, int *address, int size, usr_callback user_routine, long tag) {
    return dis_add_service(name, description, address, size, user_routine, tag);
}

int w_dis_update_service(unsigned int service_id) {
    return dis_update_service(service_id);
}
int w_dis_start_serving(const char *task_name) {
    return dis_start_serving(task_name);
}

int w_dis_remove_service(unsigned int service_id) {
    return dis_remove_service(service_id);
}