#ifndef _DIM_WRAPPER_H
#define _DIM_WRAPPER_H

typedef void (*cmd_callback)(long *tag, char *cmnd_buffer, int *size);
typedef void (*usr_callback)(long *tag, int ** adress, int *size);
extern unsigned int w_dis_add_service(const char *name, const char *description, int *address, int size, usr_callback user_routine, long tag);
extern int w_dis_update_service(unsigned int service_id);
extern unsigned int w_dis_add_cmnd(const char *name, const char *description, cmd_callback user_routine, long tag);
extern int w_dis_start_serving(const char *task_name);
extern int w_dis_remove_service(unsigned int service_id);

#endif
